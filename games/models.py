from django.db import models
from producers.models import Producer
from categories.models import Category
from platforms.models import Platform


class Game(models.Model):
    title = models.CharField(max_length=128, blank=False, null=False)
    description = models.TextField(default="", blank=False, null=False)
    year = models.DateField(blank=False, null=False)
    cover = models.ImageField(upload_to="games_covers", blank=False, null=False)
    producer = models.ForeignKey(Producer, on_delete=models.DO_NOTHING, blank=False, null=False)
    category = models.ManyToManyField(Category)
    platforms = models.ManyToManyField(Platform)

    def __str__(self):
        return self.title
