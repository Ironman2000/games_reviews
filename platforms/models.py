from django.db import models


class Platform(models.Model):
    name = models.CharField(max_length=64, blank=False, null=False)
    shortcut = models.CharField(max_length=10, blank=False, null=False)
    icon = models.CharField(max_length=64, blank=False, null=False)

    def __str__(self):
        return self.name
