from django.db import models


class Producer(models.Model):
    name = models.CharField(max_length=128, blank=False, null=False)
    birth = models.DateField(blank=False, null=False)
    country = models.CharField(max_length=64)
    description = models.TextField(default="", blank=False, null=False)

    def __str__(self):
        return self.name
